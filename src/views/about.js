import {
  Grid,
  Header,
  Image,
  Label,
  Tab,
  List,
  Icon,
  Flag,
} from "semantic-ui-react";
import React from "react";

const AboutMePane = () => (
  <Grid className="piledSegment" columns={2} divided>
    <Grid.Row>
      <Grid.Column>
        <Header as="h3" textAlign="left" className="playFair">
          Hello
          <Header as="h1" textAlign="left" className="playFair">
            I'm Deepak Chauhan, welcome to my space.
          </Header>
        </Header>
        <br />
        <List>
          <List.Item>
            <Icon name="child" size="big" />
            <List.Content>
              <List.Header as={"a"}>Born & Raised in</List.Header>
              <List.Description>
                New Delhi, India <Flag name="in" />
              </List.Description>
            </List.Content>
          </List.Item>
          <br />
          <List.Item>
            <Icon name="location arrow" size="big" />
            <List.Content>
              <List.Header as={"a"}>Currently at</List.Header>
              <List.Description>
                New Delhi, India <Flag name="in" />
              </List.Description>
            </List.Content>
          </List.Item>
          <br />
        </List>
      </Grid.Column>
      <Grid.Column textAlign="center">
        <Image src="profile.jpg" size="medium" circular centered />
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

const AboutMe = () => (
  <Tab.Pane className="piled tabContent" color="blue" attached={false}>
    <Label as="a" color="blue" className="playFair" ribbon>
      About Me
    </Label>
    <br />
    <br />
    <AboutMePane />
  </Tab.Pane>
);
export default AboutMe;
