import React from "react";
import { Grid, Header } from "semantic-ui-react";
import { Tab } from "semantic-ui-react";
import AboutMe from "./about";
import Timeline from "./timeline";
import Social from "./social";
import Contact from "./contact";

const panes = [
  {
    menuItem: "About",
    render: () => <AboutMe />,
  },
  {
    menuItem: "Timeline",
    render: () => <Timeline />,
  },
  {
    menuItem: "Social",
    render: () => <Social />,
  },
  {
    menuItem: "Contact",
    render: () => <Contact />,
  },
];

const PageHeader = () => (
  <div>
    <Grid centered columns={1}>
      <Grid.Column textAlign="center" className="system-title">
        <Header as="h1" textAlign="center" className="playFair">
          Deepak Chauhan
        </Header>
      </Grid.Column>
    </Grid>
    <Grid centered columns={1}>
      <Grid.Column>
        <Tab
          menu={{
            secondary: true,
            tabular: true,
            style: {
              display: "flex",
              justifyContent: "center",
            },
          }}
          panes={panes}
        />
      </Grid.Column>
    </Grid>
  </div>
);

export default PageHeader;
