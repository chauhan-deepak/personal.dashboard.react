import { Grid, Header, Image, Label } from "semantic-ui-react";
import React from "react";

const reactImageProps = {
  avatar: true,
  spaced: "right",
  src: "logo192.png",
  className: "rotate",
};

const sUIImageProps = {
  avatar: true,
  spaced: "right",
  src: "suiLogo.png",
  className: "rotate",
};

const PageFooter = () => (
  <footer>
    <Grid centered columns={1}>
      <Grid.Column textAlign="center" className="system-title">
        <Header as="h5" textAlign="center">
          Powered by <br /> <br />
          <Label
            as="a"
            href="https://reactjs.org/"
            target="_blank"
            content="React"
            image={reactImageProps}
          />
          &nbsp; and
          <Label
            as="a"
            href="https://semantic-ui.com/"
            target="_blank"
            content="Semantic"
            image={sUIImageProps}
          />
          <br />
          <br />
          Fork this on &nbsp;&nbsp;&nbsp;
          <Label
            as="a"
            tag
            image
            color={"blue"}
            href="https://bitbucket.org/chauhan-deepak/personal.dashboard.react/fork"
            target="_blank"
          >
            &nbsp;
            <Image src="bitbucket-brands.png" size={"mini"} />
            Bitbucket
          </Label>
          <br />
          <br />
          Last updated on 18th of July 2021
        </Header>
      </Grid.Column>
    </Grid>
  </footer>
);

export default PageFooter;
