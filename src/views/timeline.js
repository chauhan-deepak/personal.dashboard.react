import {
  Button,
  Card,
  Dimmer,
  Image,
  Label,
  List,
  Loader,
  Tab,
} from "semantic-ui-react";
import React, { useState } from "react";

const WorkLabel = ({ level }) => (
  <Label className={level} attached="top right">
    <List>
      <List.Item>
        <List.Icon name="suitcase" />
        <List.Content>Work</List.Content>
      </List.Item>
    </List>
  </Label>
);

const SchoolLabel = ({ level }) => (
  <Label className={level} attached="top right">
    <List>
      <List.Item>
        <List.Icon name="book" />
        <List.Content>School</List.Content>
      </List.Item>
    </List>
  </Label>
);

const Event = ({ src, title, meta, description }) => {
  const [loaded, setLoaded] = useState(false);
  return (
    <Dimmer.Dimmable as={Card} dimmed={!loaded} src={src} color={"red"}>
      <Dimmer active={!loaded}>
        <Image>
          <Loader />
        </Image>
      </Dimmer>
      <Image src={src} wrapped ui={false} onLoad={() => setLoaded(true)} />
      <Card.Content>
        <Card.Header>{title}</Card.Header>
        <Card.Description>{description}</Card.Description>
        <Card.Meta>
          <span className="date">{meta}</span>
        </Card.Meta>
      </Card.Content>
      <Card.Content extra>
        <div className="ui two buttons">
          <Button basic color="green">
            View
          </Button>
        </div>
      </Card.Content>
    </Dimmer.Dimmable>
  );
};

const T2B = () => (
  <Tab.Pane className={"timelineTab"}>
    <WorkLabel level={"timelineLabelTen"} />
    <Event
      src="t2b.webp"
      title="Senior Software Developer"
      description="Thoughts2binary"
      meta="Joined at 2019"
    />
    <br />
  </Tab.Pane>
);

const SRM = () => (
  <Tab.Pane>
    <SchoolLabel level={"timelineLabelNine"} />
  </Tab.Pane>
);

const DAV = () => (
  <Tab.Pane>
    <SchoolLabel level={"timelineLabelEight"} />
  </Tab.Pane>
);

const panes = [
  { menuItem: "2019-2021", render: () => <T2B /> },
  { menuItem: "2015-2019", render: () => <SRM /> },
  { menuItem: "2013-2015", render: () => <DAV /> },
];

const TimelinePane = () => (
  <Tab
    menu={{
      fluid: true,
      vertical: true,
    }}
    menuPosition="right"
    panes={panes}
  />
);

const Timeline = () => (
  <Tab.Pane className="piled tabContent" color="red" attached={false}>
    <Label as="a" color="red" className="playFair" ribbon>
      Timeline
    </Label>
    <br />
    <br />
    <TimelinePane />
  </Tab.Pane>
);
export default Timeline;
