import { Grid, Icon, Label, Tab } from "semantic-ui-react";
import React from "react";

const ContactPane = () => (
  <Grid className="piledSegment" columns={2} divided>
    <Grid.Row>
      <Grid.Column>
        <Icon loading name="cogs" size="big"></Icon>
        &nbsp; Under Construction
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

const Contact = () => (
  <Tab.Pane className="piled tabContent" color="pink" attached={false}>
    <Label as="a" color="pink" className="playFair" ribbon>
      Contact
    </Label>
    <br />
    <br />
    <ContactPane />
  </Tab.Pane>
);
export default Contact;
