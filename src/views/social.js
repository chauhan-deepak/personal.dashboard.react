import { Grid, Icon, Label, Tab } from "semantic-ui-react";
import React from "react";

const SocialPane = () => (
  <Grid className="piledSegment" columns={2} divided>
    <Grid.Row>
      <Grid.Column>
        <Icon loading name="cogs" size="big"></Icon>
        &nbsp; Under Construction
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

const Social = () => (
  <Tab.Pane className="piled tabContent" color="green" attached={false}>
    <Label as="a" color="green" className="playFair" ribbon>
      Social
    </Label>
    <br />
    <br />
    <SocialPane />
  </Tab.Pane>
);
export default Social;
