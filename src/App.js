import PageHeader from "./views/header";
import PageFooter from "./views/footer";
import "./App.css";

function App() {
  return (
    <div>
      <header>
        <PageHeader />
        <br />
        <br />
        <br />
        <PageFooter />
      </header>
    </div>
  );
}

export default App;
